﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CookiesCRUD.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous" />
    <link href="Styles/styles.css" rel="stylesheet" />

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="form">
                <h2 class="text-center">Log in</h2>
                <div class="form-group">
                    <asp:TextBox ID="emailID" CssClass="form-control" TextMode="Email" required="required"
                        placeholder="Enter email" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="passwordID" CssClass="form-control" TextMode="Password" required="required"
                        placeholder="Password" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Button ID="submit" CssClass="btn btn-primary btn-block" runat="server" Text="Login"
                        OnClick="submit_Click" />
                </div>
                <asp:Button ID="signup" CssClass="btn btn-outline-secondary btn-block"
                    Text="Create an Account" runat="server" OnClick="signup_Click" formnovalidate="formnovalidate"/>
            </div>
        </div>
    </form>
</body>
</html>
