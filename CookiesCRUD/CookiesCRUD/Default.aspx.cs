﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CookiesCRUD
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (Page.IsValid)
                {
                    HttpCookie cookie = Request.Cookies.Get("cookie");
                    if (cookie == null)
                        cookie = new HttpCookie("cookie");
                    if (cookie["emailID"] == null || cookie["password"] == null)
                    {
                        Response.Write("<script language = javascript>alert('Account does not exist');</script>");
                        return;

                    }
                    else if (cookie["emailID"] == emailID.Text && cookie["password"] == passwordID.Text)
                    {
                        Response.Redirect("~/Home.aspx");
                    }
                    if(cookie["emailID"] != emailID.Text && cookie["password"] != passwordID.Text)
                    {
                        Response.Write("<script language = javascript>alert('Login failed');</script>");
                    }
                }
            }
        }

        protected void signup_Click(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                Response.Redirect("~/SignUp.aspx");
            }
        }
    }
}