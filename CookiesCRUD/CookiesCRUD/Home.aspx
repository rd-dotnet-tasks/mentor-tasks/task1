﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="CookiesCRUD.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
    <link href="Styles/styles.css" rel="stylesheet" />

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbar" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Logo</a>
                <div class="collapse navbar-collapse justify-content-between" id="navbar">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="#">Home</a>
                    </div>
                    <div class="navbar-nav">
                        <asp:LinkButton CssClass="nav-item nav-link" Text="Log Out" OnClick="logout_Click" runat="server" />
                    </div>
                </div>
            </nav>
            <dl class="row ml-3 mt-3">
                <dt class="col-sm-3">User Name</dt>
                <dd class="col-sm-9" id="userName" runat="server"></dd>

                <dt class="col-sm-3">Email</dt>
                <dd class="col-sm-9" id="email" runat="server"></dd>

                <dt class="col-sm-3">Gender</dt>
                <dd class="col-sm-9" id="gender" runat="server"></dd>
             
                <dt class="col-sm-3">City</dt>
                <dd class="col-sm-9" id="city" runat="server"></dd>
            </dl>
        </div>
    </form>
</body>
</html>
