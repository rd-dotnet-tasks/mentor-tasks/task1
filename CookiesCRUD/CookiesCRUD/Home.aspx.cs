﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CookiesCRUD
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userName.InnerText = Request.Cookies.Get("cookie")["userName"];
                email.InnerText = Request.Cookies.Get("cookie")["emailID"];
                gender.InnerText = Request.Cookies.Get("cookie")["gender"];
                city.InnerText = Request.Cookies.Get("cookie")["city"];
            }
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies.Get("cookie");
            cookie.Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies.Add(cookie);
            Response.Redirect("~/Default.aspx");
        }
    }
}