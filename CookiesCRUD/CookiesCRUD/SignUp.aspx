﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="CookiesCRUD.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous">
    <link href="Styles/styles.css" rel="stylesheet" />

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="form">
            <h2 class="text-center">Sign Up</h2>
            <div class="form-group row">
                <label for="userName" class="col-sm-2 col-form-label">User Name</label>
                <div class="col-sm-10">
                    <asp:TextBox CssClass="form-control" ID="userName" placeholder="User Name"
                        required="required" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <label for="emailID" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <asp:TextBox TextMode="Email" CssClass="form-control" ID="emailID" placeholder="Email"
                        required="required" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <asp:TextBox TextMode="Password" CssClass="form-control" ID="password" placeholder="Password"
                        required="required" runat="server"></asp:TextBox>
                </div>
            </div>
            <fieldset class="form-group">
                <div class="row">
                    <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <asp:RadioButton ID="male" CssClass="form-check-input" GroupName="gender" runat="server" />
                            <asp:Label ID="male_text" CssClass="form-check-label" AssociatedControlID="male" Text="Male" runat="server"></asp:Label>
                        </div>
                        <div class="form-check">
                            <asp:RadioButton ID="female" CssClass="form-check-input" GroupName="gender" runat="server" />
                            <asp:Label ID="female_text" CssClass="form-check-label" AssociatedControlID="female" Text="Female"
                                runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group row">
                <div class="col-sm-2">City</div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <asp:DropDownList ID="city" CssClass="form-control" runat="server">
                            <asp:ListItem Value="">Choose...</asp:ListItem>
                            <asp:ListItem>Hyderabad</asp:ListItem>
                            <asp:ListItem>Chennai</asp:ListItem>
                            <asp:ListItem>Mumbai</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <asp:Button ID="submit" CssClass="btn btn-primary btn-block" Text="Sign Up" OnClick="submit_Click" runat="server" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
