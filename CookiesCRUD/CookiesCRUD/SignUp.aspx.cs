﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CookiesCRUD
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (Page.IsValid)
                {
                    HttpCookie cookie = Request.Cookies.Get("cookie");
                    if (cookie == null)
                        cookie = new HttpCookie("cookie");
                    cookie["userName"] = userName.Text;
                    cookie["emailID"] = emailID.Text;
                    cookie["password"] = password.Text;
                    if (male.Checked)
                        cookie["gender"] = male_text.Text;
                    else cookie["gender"] = female_text.Text;
                    cookie["city"] = city.SelectedValue;
                    cookie.Expires = DateTime.Now.AddMinutes(1);
                    Response.Cookies.Add(cookie);
                    Response.Redirect("~/Default.aspx");
                }
            }
        }
    }
}